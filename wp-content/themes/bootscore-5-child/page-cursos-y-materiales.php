<?php get_header(); ?>

<section id="category">
	<div class="container">
		<div class="row">

			<div class="col-xl-12 text-center">
				<h1><?php the_title();?></h1>
			</div>
			<?php
				$terms = get_terms( array(
				    'taxonomy' => 'product_cat',
				    'hide_empty' => false,
				    'parent' => 0
				));
			?>

			<?php foreach ($terms as $term): ?>

				<?php if ($term->term_id != 15): ?>

					<div class="col-xl-4 text-center">
						<?php
							//Image
							$thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
							$image = wp_get_attachment_url( $thumbnail_id );
							$url = get_term_link( $term );
						?>
						<div class="img" style="background-image: url('<?php echo $image;?>');">
							<div class="text">
								<h2><?php echo $term->name;?></h2>
							</div>
						</div>
						<a href="<?php echo $url;?>" class="btn btn-grey">Ver más</a>
					</div>

				<?php endif ?>

			<?php endforeach ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>