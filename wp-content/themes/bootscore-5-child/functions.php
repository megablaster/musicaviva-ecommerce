<?php

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

function add_to_cart_variation($product_id,$variation_id,$constancia,$variation){

    $product_id   = $product_id; //57
    $quantity     = 1;
    $variation_id = $variation_id; //212
    $variation    = array(
        'Constancia' => $constancia, //Básico
    );

    WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation );
}

// style and scripts
 add_action( 'wp_enqueue_scripts', 'bootscore_5_child_enqueue_styles' );
 function bootscore_5_child_enqueue_styles() {
     
     // style.css
    wp_enqueue_style( 'fancybox-style', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css' );
    wp_enqueue_style( 'owl-style', get_stylesheet_directory_uri() . '/script/assets/owl.carousel.min.css' );
    wp_enqueue_style( 'owl-theme-style', get_stylesheet_directory_uri() . '/script/assets/owl.theme.default.min.css' );
     wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
     
     // custom.js
     wp_enqueue_script('fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', false, '', true);
     wp_enqueue_script('owl-js', get_stylesheet_directory_uri() . '/script/owl.carousel.min.js', false, '', true);
     wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true);

 } 

 add_filter( 'woocommerce_account_menu_items', 'bbloomer_rename_address_my_account', 9999 );
 
function bbloomer_rename_address_my_account( $items ) {
   $items = array(
    'dashboard'       => __( 'Dashboard', 'woocommerce' ),
    'orders'          => __( 'Orders', 'woocommerce' ),
    'downloads'       => __( 'Cursos', 'woocommerce' ),
    'payment-methods' => __( 'Payment methods', 'woocommerce' ),
    'edit-account'    => __( 'Account details', 'woocommerce' ),
    'customer-logout' => __( 'Logout', 'woocommerce' ),
    );
   return $items;
}


function add_to_cart_form_shortcode( $atts ) {
     if ( empty( $atts ) ) {
         return '';
     }

     if ( ! isset( $atts['id'] ) && ! isset( $atts['sku'] ) ) {
         return '';
     }

     $args = array(
         'posts_per_page'      => 1,
         'post_type'           => 'product',
         'post_status'         => 'publish',
         'ignore_sticky_posts' => 1,
         'no_found_rows'       => 1,
     );

     if ( isset( $atts['sku'] ) ) {
         $args['meta_query'][] = array(
             'key'     => '_sku',
             'value'   => sanitize_text_field( $atts['sku'] ),
             'compare' => '=',
         );

         $args['post_type'] = array( 'product', 'product_variation' );
     }

     if ( isset( $atts['id'] ) ) {
         $args['p'] = absint( $atts['id'] );
     }

     $single_product = new WP_Query( $args );

     $preselected_id = '0';


     if ( isset( $atts['sku'] ) && $single_product->have_posts() && 'product_variation' === $single_product->post->post_type ) {

         $variation = new WC_Product_Variation( $single_product->post->ID );
         $attributes = $variation->get_attributes();


         $preselected_id = $single_product->post->ID;


         $args = array(
             'posts_per_page'      => 1,
             'post_type'           => 'product',
             'post_status'         => 'publish',
             'ignore_sticky_posts' => 1,
             'no_found_rows'       => 1,
             'p'                   => $single_product->post->post_parent,
         );

         $single_product = new WP_Query( $args );
     ?>
         <script type="text/javascript">
             jQuery( document ).ready( function( $ ) {
                 var $variations_form = $( '[data-product-page-preselected-id="<?php echo esc_attr( $preselected_id ); ?>"]' ).find( 'form.variations_form' );
                 <?php foreach ( $attributes as $attr => $value ) { ?>
                     $variations_form.find( 'select[name="<?php echo esc_attr( $attr ); ?>"]' ).val( '<?php echo esc_js( $value ); ?>' );
                 <?php } ?>
             });
         </script>
     <?php
     }

     $single_product->is_single = true;
     ob_start();
     global $wp_query;

     $previous_wp_query = $wp_query;

     $wp_query          = $single_product;

     wp_enqueue_script( 'wc-single-product' );
     while ( $single_product->have_posts() ) {
         $single_product->the_post()
         ?>
         <div class="single-product" data-product-page-preselected-id="<?php echo esc_attr( $preselected_id ); ?>">
             <?php woocommerce_template_single_add_to_cart(); ?>
         </div>
         <?php
     }

     $wp_query = $previous_wp_query;

     wp_reset_postdata();
     return '<div class="woocommerce">' . ob_get_clean() . '</div>';
}
add_shortcode( 'add_to_cart_form', 'add_to_cart_form_shortcode' );