<?php get_header(); ?>
<div class="container" id="course-single">
	<div class="row">
		<div class="col-xl-12">
			<h2><?php the_title();?></h2>
			<?php the_content(); ?>
			<a href="<?php echo home_url().'/mi-cuenta/cursos';?>" class="btn btn-primary">Regresar a cursos</a>
		</div>
	</div>
</div>
<?php get_footer(); ?>