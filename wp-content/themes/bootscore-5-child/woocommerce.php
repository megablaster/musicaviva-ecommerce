<?php get_header(); $current = get_queried_object(); ?>

<?php if (is_shop()): ?>
    <section id="category">
        <div class="container">
            <div class="row">

                <?php
                    $terms = get_terms( array(
                        'taxonomy' => 'product_cat',
                        'hide_empty' => false,
                        'parent' => 0
                    ));
                ?>

                <?php foreach ($terms as $term): ?>

                    <?php if ($term->term_id != 15): ?>

                        <div class="col-xl-4 col-lg-6 col-md-6 text-center">
                            <?php
                                //Image
                                $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
                                $image = wp_get_attachment_url( $thumbnail_id );
                                $url = get_term_link( $term );
                            ?>

                            <div class="img" style="background-image: url('<?php echo $image;?>');">
                                <div class="blue">
                                    <h2><?php echo $term->name;?></h2>
                                </div>
                            </div>
                            <a href="<?php echo $url;?>" class="btn btn-grey">Ver más</a>
                        </div>

                    <?php endif ?>

                <?php endforeach ?>
            </div>
        </div>
    </section>
<?php endif ?>

<!--Categorías-->
<?php if(is_product_category()): ?>

    <?php
        $terms = get_terms(
            'product_cat',
            array(
                'hide_empty' => false,
                'child_of' => $current->term_id
            )
        );
    ?>

    <?php if ($terms): ?>

        <section id="category">
            <div class="container">
                <div class="row">

                    <div class="col-xl-12 text-center">
                        <h1><?php echo $current->name;?></h1>
                    </div>

                    <?php foreach ($terms as $term): ?>

                        <?php if ($term->term_id != 15): ?>

                            <div class="col-xl-3 ol-lg-6 col-md-6 text-center">
                                <?php
                                    //Image
                                    $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
                                    $image = wp_get_attachment_url( $thumbnail_id );
                                    $url = get_term_link( $term );
                                ?>
                                <img src="<?php echo $image;?>" class="img-fluid">
                                <div class="blue">
                                    <h2>
                                        <?php echo $term->name;?>
                                        <?php if (get_field('modulos',$term)): ?>
                                            <span>(<?php the_field('modulos',$term);?>)</span>
                                        <?php endif ?>
                                    </h2>
                                </div>

                                <?php if ($term->count != 0): ?>
                                   <a href="<?php echo $url;?>" class="btn btn-grey">Ver más</a>
                                <?php endif ?>
                            </div>

                        <?php endif ?>

                    <?php endforeach ?>
                </div>
            </div>
        </section>

    <?php else: ?>

         <div id="curriculum-popup" style="display: none;max-width: 850px;">
            <img src="<?php echo get_stylesheet_directory_uri().'/img/elda.png';?>" class="img-fluid elda">
            <h3>Elda Nelly Treviño Flores, Ph.D.</h3>
            <div class="line"></div>
            <p>Catedrática y colaboradora del cuerpo académico "Fomento, Promoción y Vinculación de la Música" de la Facultad de Música de la UANL, catedrática y coordinadora general de Programas Dalcroze enla Universidad Panamericana y directora de la escuela independiente "MúsicaViva".</p>
            <p>Conferencista, pedagoga y asesora académica dentro y fuera de México. De 2012-16 fue coordinadora general del programa de certificación Dalcroze en el Conservatorio de las Rosas. Ha sido pianista solista con la OSUANL, orquestas sinfónicas de Matanzas y Santiago en Cuba manteniéndose activa como pianista colaboradora y promotora musical.</p>
            <p>Doctora en Filosofía con orientación en Psicología(Summa Cum Laude) UANL y candidata a doctor en Armonía Modal y Atonal por la Universidad Tito Puente. Cuenta con Licenciatura y Maestría en Música con especialidad en Literatura y Pedagogía Pianística en la Universidad de Texas en Austin; Certificado y Licencia en Rítmica Jaques-Dalcroze por la Carnegie Mellon University en Pittsburgh, PA. Entrelas distinciones recibidas destacan Premio a las Artesen Artes Auditivas, UANL(2009), Programa de Investigaciones Conjuntas Matías Romero (SRE), FORCA Noreste, becas de mérito académico en UDEM, UT Austin, Panamerican Round Table y MTNA Foundation, entreotras</p>
            <p>Actualmente es representante en México ante la Federación Internacional de Maestros de Rítmica (FIER)con sede en Suiza y miembro del comité de publicaciones de la Dalcroze Society of America en Estados Unidos.</p>
        </div>

        <section id="category" class="info-category">

            <?php if ($current->slug != 'cursos-especializados'): ?>
            
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <?php woocommerce_breadcrumb(); ?>
                        </div>
                        <div class="col-xl-4">
                            <?php
                                $thumbnail_id = get_term_meta( $current->term_id, 'thumbnail_id', true );
                                $image = wp_get_attachment_url( $thumbnail_id );
                                $url = get_term_link( $current );
                            ?>

                            <?php if (get_field('video')): ?>
                                <video width="100%" controls>
                                    <source src="<?php the_field('video');?>" type="video/mp4">
                                    Lo sentimos tu navegador es muy antiguo, recomendamos actualizarlo.
                                </video>
                            <?php else:?>
                                <img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
                            <?php endif ?>

                        </div>
                        <div class="col-xl-8">
                            <?php echo do_shortcode('[shop_messages]');?>
                            <h3><?php echo $current->name;?></h3>
                            <?php if (get_field('impartido',$current)): ?>
                                <h4><strong>Impartido por:</strong> <?php the_field('impartido',$current);?></h4>
                            <?php endif ?>
                            <?php if (get_field('curriculum',$current)): ?>
                                <a href="#curriculum-popup" data-fancybox>Ver currículum</a>
                            <?php endif ?>
                            
                            <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'orderby' => 'ID',
                                    'order' => 'ASC',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'terms'    =>  $current->slug,
                                            'field'    => 'slug',
                                            'operator' => 'IN'
                                        )
                                    )
                                );
                                $qq = new WP_Query($args);
                            ?>

                            <?php if (get_field('libro',$current) == 'si'): ?>
                                  <div class="row book">
                                    <div class="col-xl-12">
                                         <?php while($qq->have_posts()): $qq->the_post() ?>
                                            <?php
                                                $prod = wc_get_product( $post->ID );
                                            ?>
                                            <?php echo do_shortcode( '[add_to_cart id=' . $prod->id . ']' ) ?>
                                        <?php endwhile ?>
                                    </div>
                                </div>
                            <?php endif ?>
                          
                            <?php if (get_field('descripcion',$current)): ?>
                                <?php the_field('descripcion',$current);?>
                            <?php endif ?>
                            <div class="accordion" id="accordionExample">
                              <?php if (get_field('dirigido',$current)): ?>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Dirigido a
                                      </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                      <div class="accordion-body">
                                        <?php the_field('dirigido',$current);?>
                                      </div>
                                    </div>
                                  </div>
                              <?php endif ?>
                              <?php if (get_field('acceso_al_contenido',$current)): ?>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <?php if (get_field('libro',$current) == 'si'): ?>
                                            Acceso al libro
                                        <?php else: ?>
                                            Acceso al contenido
                                        <?php endif ?>
                                      </button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                      <div class="accordion-body">
                                        <?php the_field('acceso_al_contenido',$current);?>
                                      </div>
                                    </div>
                                  </div>
                              <?php endif ?>
                              <?php if (get_field('forma_de_pago',$current)): ?>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingThree">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Forma de pago
                                      </button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                      <div class="accordion-body">
                                       <?php the_field('forma_de_pago',$current);?>
                                      </div>
                                    </div>
                                  </div>
                              <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endif ?>

            <?php if (get_field('libro',$current) != 'si'): ?>

                <div class="container-fluid" style="max-width: 70%;">
                    <div class="row" id="related-course">
                        <?php
                            $args = array(
                                'post_type' => 'product',
                                'orderby' => 'ID',
                                'order' => 'ASC',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'terms'    =>  $current->slug,
                                        'field'    => 'slug',
                                        'operator' => 'IN'
                                    )
                                )
                            );
                            $q = new WP_Query($args);
                        ?>

                        <div class="row">
                            <?php while($q->have_posts()): $q->the_post() ?>
                                <?php
                                    global $product;
                                ?>
                                <div class="col-xl-4 col-lg-6 text-center item-related">
                                    <div class="text">
                                        <div class="inner">
                                            <h4><?php the_title();?></h4>
                                            <?php the_excerpt();?>
                                        </div>
                                    </div>
                                    <?php if (get_field('video')): ?>
                                        <video width="100%" controls>
                                            <source src="<?php the_field('video');?>" type="video/mp4">
                                            Lo sentimos tu navegador es muy antiguo, recomendamos actualizarlo.
                                        </video>
                                    <?php else:?>
                                        <img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
                                    <?php endif ?>
                                    
                                    <div class="price">
                                        <?php echo $product->get_price_html(); ?>
                                    </div>
                                    <?php if ($current->slug != 'cursos-especializados'): ?>
                                        <a href="#modal-<?php echo $post->ID;?>" data-fancybox class="btn">Seleccionar opciones</a>
                                    <?php endif ?>                                    
                                </div>

                                <div id="modal-<?php echo $post->ID;?>" style="display: none;max-width: 70%;">

                                    <div class="row">
                                        <div class="col-xl-5">

                                             <?php if (get_field('video')): ?>
                                                <video width="100%" controls>
                                                    <source src="<?php the_field('video');?>" type="video/mp4">
                                                    Lo sentimos tu navegador es muy antiguo, recomendamos actualizarlo.
                                                </video>
                                            <?php else:?>
                                                <img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
                                            <?php endif ?>

                                        </div>
                                        <div class="col-xl-7">

                                            <?php
                                                global $product;
                                                echo wc_print_notices();
                                            ?>

                                            <div class="text">
                                                <h3><?php echo $current->name;?> - <?php the_title();?></h3>
                                                <?php the_excerpt(); ?>
                                                <?php if (get_field('descripcion',$current)): ?>
                                                    <?php the_field('descripcion',$current);?>
                                                <?php endif ?>

                                                <?php if ( $product->is_type( 'variable' )):?>

                                                
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <?php do_action( 'woocommerce_before_single_product' ); ?>
                                                            <?php do_action( 'woocommerce_single_product_summary' ); ?>
                                                            <?php do_action( 'woocommerce_after_single_product_summary' );?>
                                                        </div>
                                                    </div>

                                                <?php else:?>

                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <?php do_action( 'woocommerce_before_single_product' ); ?>
                                                            <?php do_action( 'woocommerce_single_product_summary' ); ?>
                                                            <?php do_action( 'woocommerce_after_single_product_summary' );?>
                                                        </div>    
                                                    </div>

                                                <?php endif ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile ?>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </section>

    <?php endif ?>

<?php endif ?>

<!--Producto-->
<?php if(is_product()): ?>

    <div class="container" id="single-product">
        <div class="row">
            <div class="col-12">
                <?php woocommerce_content(); ?>
            </div>
        </div>
    </div>

<?php endif ?>
<div class="clearfix"></div>
<?php get_footer(); ?>

<script>
    jQuery(document).ready(function($){

        $('[data-fancybox]').fancybox({
        });

    });
    
</script>