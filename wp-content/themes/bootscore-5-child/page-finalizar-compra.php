<?php get_header(); ?>
<section id="checkout-page">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<?php while(have_posts()): the_post() ?>
					<?php the_content();?>
				<?php endwhile ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>