jQuery(document).ready(function ($) {

    $('.select-product').on('change', function(e){
        e.preventDefault();

        var url = $(this).val();
        var id = $(this).children('option:selected').data('id');

        $('#add_to_cart_primary-'+id).attr('href',url);

    });

    $('.select_variation').on('change', function(e){
        e.preventDefault();

        var url = $(this).val();
        var id = $(this).data('id');
        $('#add-to-cart-'+ id).attr('href',url);

    });

    $('.owl-related').owlCarousel({
        loop:true,
        margin:30,
        nav:false,
        autoplay:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })

}); // jQuery End
