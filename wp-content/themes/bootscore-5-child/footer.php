<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootscore
 */

?>

            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 text-center">
                            <p>Copyright &copy; - <?php bloginfo('name');?></p>      
                        </div>
                    </div>
                </div>
            </footer>

            <div class="top-button position-fixed zi-1020">
                <a href="#to-top" class="btn btn-primary shadow"><i class="fas fa-chevron-up"></i></a>
            </div>

            <a href="<?php echo home_url().'/mi-cuenta/cursos/';?>" class="btn btn-courses">Mis cursos</a>

        </div><!-- #page -->

        <?php wp_footer(); ?>

    </body>
</html>
