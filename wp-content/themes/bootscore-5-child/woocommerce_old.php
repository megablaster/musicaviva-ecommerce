    <div class="col-xl-4">
    </div>
    <div class="col-xl-8">
        <h3><?php the_title();?></h3>
        <h4><strong>Impartido por:</strong> <?php the_field('impartido',$current);?></h4>
        <a href="<?php the_field('curriculum',$current);?>" target="_blank" download>Ver curriculum</a>
        <?php the_field('descripcion',$current);?>
        <div class="accordion" id="accordionExample">
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
              <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Dirigido a
              </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <?php the_field('dirigido',$current);?>
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Acceso al contenido
              </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <?php the_field('acceso_al_contenido',$current);?>
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Forma de pago
              </button>
            </h2>
            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
              <div class="accordion-body">
               <?php the_field('forma_de_pago',$current);?>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row" id="related-course">

        <?php while($q->have_posts()): $q->the_post() ?>

            <div class="col-xl-3 item-course">
                <?php
                    $product = wc_get_product( $post->ID );
                ?>
                <h4><?php the_title();?></h4>
                <?php echo $post->post_excerpt; ?>
                <?php echo do_shortcode('[add_to_cart id="45"]');?>
            </div>

        <?php endwhile?>

    </div>