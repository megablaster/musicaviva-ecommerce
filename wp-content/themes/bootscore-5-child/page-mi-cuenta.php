<?php get_header(); ?>
<div class="container" style="padding:30px 0;">
	<div class="row">
		<div class="col-xl-12">
			<?php the_content(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>